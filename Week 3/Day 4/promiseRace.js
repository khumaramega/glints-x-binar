// Import fs to read/write file
const fs = require('fs')

/* Start make promise */
const readFile = options => file => new Promise((resolve, reject) => {
  fs.readFile(file, options, (err, content) => {
    if (err) return reject(err)
    return resolve(content)
  })
})

const writeFile = (file, content) => new Promise((resolve, reject) => {
  fs.writeFile(file, content, err => {
    if (err) return reject(err)
    return resolve()
  })
})
/* End make promise */


/* Make options variable for fs */
const
  read = readFile('utf-8')

/* End make options variable for fs */

/* Promise race */
Promise.race([read('contents/satu.txt'), read('contents/dua.txt'), read('contents/tiga.txt'), read('contents/empat.txt'), read('contents/lima.txt'), read('contents/enam.txt'), read('contents/tujuh.txt'), read('contents/delapan.txt'), read('contents/sembilan.txt'), read('contents/sepuluh.txt')])
  .then((value) => {
    console.log(value);
  })
  .catch(error => {
    console.log(error);
  })
/* Promise race end */
