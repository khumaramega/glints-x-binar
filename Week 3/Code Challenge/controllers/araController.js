// Make HelloController that will be used in helloRoutes
class AraController {

    // This function will be called in helloRoutes
    async ara(req, res) {
      try {
        console.log('You are accessing Ara html!');
        res.render('ara.ejs') // if success, will be rendering html file from views/hello.ejs
      } catch (e) {
        res.status(500).send(exception) // if error, will be display "500 Internal Server Error"
      }
    }
  
  }
  
  module.exports = new AraController // We don't need to instance a object bacause exporting this file will be automatically to be an object
  