
const index = require(`./event.js`)
const readline = require('readline')
/* const index.rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
}) */
let daftar=[
  {name: "John", status: "suspect"}, {name: "Aratso", status: "positive"},
  {name: "Mega", status: "suspect"}, {name: "Ayu", status: "positive"},
  {name: "Dian", status: "negative"}, {name: "Khumara", status: "negative"},
   
]

function positive(){
  console.log("Positive");
  console.log("===========");
  for (let i=0; i<daftar.length; i++){
    if(daftar[i].status == "positive" ){
      console.log(`${daftar[i].name}`)
    }
    }
}

function negative(){
  console.log("Negative");
  console.log("===========");
  for (let i=0; i<daftar.length; i++){
    if(daftar[i].status == "negative" ){
      console.log(`${daftar[i].name}`)
    }
}
}

function suspect(){
  console.log("Suspect");
  console.log("===========");
  for (let i=0; i<daftar.length; i++){
    if(daftar[i].status == "suspect" ){
      console.log(`${daftar[i].name}`)
    }
}
}

function menu(){
    console.log("Covid-19 Database:");
    console.log("======");
    console.log("PILIH:");
    console.log("======");
    console.log("1.Positive");
    console.log("2.Negative");
    console.log("3.Suspect");
    console.log("4.Exit");
    index.rl.question(`Pilih: `, input => {
    switch(input){
        case `1`:
            positive()
            menu()
            break;
        case `2`:
            negative()
            menu()
            break;
        case `3`:
            suspect()
            menu()
            break;
        case `4`:
          index.rl.close();
          break;
        default:
          console.log("Sorry, option not available")
            menu()
    }
   })
}
//menu()
module.exports.menu = menu