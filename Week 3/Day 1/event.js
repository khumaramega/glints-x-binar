
const EventEmitter = require('events'); // Import
const readline = require('readline');
const positive = require(`./arraycovid.js`);

 // Initialize an instance because it is a class
const my = new EventEmitter();
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})

// Registering a listener
my.on("Login Failed", function(email) {
  // TODO: Saving the login trial count in the database
  console.log(email, "is failed to login!");
})

const user = {
  login(email, password) {
    const passwordStoredInDatabase = "123456";

    if (password !== passwordStoredInDatabase) {
      my.emit("Login Failed", email); // Pass the email to the listener
      rl.close()
    } else {
      // Do something
      console.log("Login Success!\n");
      positive.menu()
    }
  }
}

rl.question("Email: ", function(email) {
  rl.question("Password: ", function(password) {
    user.login(email, password) // Run login function
  })
})
module.exports.rl=rl