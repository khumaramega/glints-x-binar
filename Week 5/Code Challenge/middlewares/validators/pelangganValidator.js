const { pelanggan} = require('../../models')
const {
  check,
  validationResult,
  matchedData,
  sanitize
} = require('express-validator'); //form validation & sanitize form params
module.exports = {
  create: [
    //Set form validation rule
    check('nama').isString().custom(value => {
      return pelanggan.findById(value).then(b => {
        if (!b) {
          throw new Error(' nama pelanggan harus string!');
        }
      })
    }),

  ],
  update: [
    //Set form validation rule
    check('nama').custom(value => {
      return pelanggan.findById(value).then(b => {
        if (!b) {
          throw new Error('nama pelanggan gagal ditambahkan!');
        }
      })
    }),
    
  ],
};

